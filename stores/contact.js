import { defineStore } from "pinia";

export const useStore = defineStore("contactCounter", {
	state: () => ({
		recordsData: null,
		recordsDataBackup: null,
		recordsDataLength: 0,
		recordsDataBackupLength: 0,
		totalPages: 0,
		currentPage: 1,
		pageStep: 15,
		selectedRecords: [],
		isFetching: true,
		dbName: useRuntimeConfig().public.APP_DB,
		colName: "contacts",
	}),

	getters: {
		data: (state) => state.recordsData,
	},

	actions: {
		async setRecordsCount(pipeline) {
			const { getRecordsCount } = useRealmApp();

			if (!pipeline) {
				pipeline = [];
			}

			pipeline.push({ $count: "total" });

			const result = await getRecordsCount(this.dbName, this.colName, pipeline);

			if (result && result.length > 0) {
				this.recordsDataBackupLength = this.recordsDataLength = result[0].total;
			} else this.recordsDataBackupLength = this.recordsDataLength = 0;
		},

		async fetchData(match, sort) {
			this.isFetching = true;
			const { getRecords } = useRealmApp();

			const skip = (this.currentPage - 1) * this.pageStep;

			let pipeline = [];

			if (match) {
				pipeline.push({ $match: { $and: JSON.parse(JSON.stringify(match)) } });
				this.setRecordsCount(pipeline);
				pipeline.pop();
			} else {
				this.setRecordsCount(pipeline);
				pipeline.pop();
			}

			if (sort) pipeline.push({ $sort: sort });

			pipeline.push({ $skip: skip });
			pipeline.push({ $limit: this.pageStep });

			this.recordsDataBackup = this.recordsData = await getRecords(
				this.dbName,
				this.colName,
				pipeline
			);

			this.calculateNoOfPages();
			this.isFetching = false;
		},

		async findRecords(match) {
			const { getRecords } = useRealmApp();

			let pipeline = [];
			if (match) {
				pipeline.push({ $match: { $and: JSON.parse(JSON.stringify(match)) } });
			}

			return await getRecords(this.dbName, this.colName, pipeline);
		},

		calculateNoOfPages() {
			this.totalPages = Math.ceil(this.recordsDataLength / this.pageStep);
		},

		async initRecordsData() {
			if (this.recordsData === null) {
				await this.fetchData();
				this.calculateNoOfPages();

				return this.recordsData;
			} else return this.recordsData;
		},

		selectRecord(id) {
			this.selectedRecords.push(id);
		},

		unselectRecord(id) {
			const index = this.selectedRecords.indexOf(id);
			this.selectedRecords.splice(index, 1);
		},

		async findByField(field, value) {
			if (!value) return null;
			if (this.recordsData) {
				return this.recordsData.find((item) => item[field] === value);
			} else {
				await this.initRecordsData();
				return this.recordsData.find((item) => item[field] === value);
			}
		},

		async findById(id) {
			if (!id) return null;
			if (this.recordsData) {
				return this.recordsData.find(
					(item) => item._id.toString() === id.toString()
				);
			} else {
				await this.initRecordsData();
				return this.recordsData.find(
					(item) => item._id.toString() === id.toString()
				);
			}
		},

		async assignCallback(header, assign_to_id, globalStore) {
			let store;
			if (globalStore[header.get_from]) {
				store = globalStore[header.get_from].useStore();
				const record = await store.findById(header.assigned_value);
				if (record && record[header.assign_to_field]) {
					record[header.assign_to_field].push(assign_to_id);
				} else if (record && !record[header.assign_to_field]) {
					record[header.assign_to_field] = [assign_to_id];
				}
				store.updateRecord(record);
			}
		},

		async addRecord(data, assign_headers, globalStore) {
			const { addRecordToDB } = useRealmApp();

			const { insertedId } = await addRecordToDB(
				this.dbName,
				this.colName,
				data
			);
			data._id = insertedId;
			this.recordsData.push(data);
			this.recordsDataBackup = this.recordsData;
			this.selectedRecords = [];
			this.recordsDataBackupLength = this.recordsDataLength =
				this.recordsDataLength + 1;

			if (assign_headers && assign_headers.length > 0) {
				assign_headers.forEach((header) =>
					this.assignCallback(header, insertedId, globalStore)
				);
			}
		},

		async updateRecord(data) {
			const { updateRecordFromDB } = useRealmApp();

			updateRecordFromDB(this.dbName, this.colName, data)
				.then((result) => {
					console.log(result);
					const index = this.recordsData.findIndex(
						(element) => data._id.toString() === element._id.toString()
					);
					console.log(data, this.selectedRecords, index);
					this.recordsData[index] = data;
					this.recordsDataBackup = this.recordsData;
					this.selectedRecords = [];
				})
				.catch((err) => {
					console.log(err);
				});
		},

		async addField(set_data, record, assign_headers) {
			const { updateRecordFromDB2 } = useRealmApp();
			const globalStore = useGlobalStore();

			this.isFetching = true;

			await updateRecordFromDB2(this.dbName, this.colName, record._id, set_data)
				.then((result) => {
					console.log(result);
					const index = this.recordsData.findIndex(
						(element) => record._id.toString() === element._id.toString()
					);

					for (const key in set_data) {
						console.log(key);
						this.recordsData[index][key] = set_data[key];
					}

					this.recordsDataBackup = this.recordsData;
					this.selectedRecords = [];
				})
				.catch((err) => {
					console.log(err);
				});

			if (assign_headers?.length > 0) {
				assign_headers.forEach((header) =>
					this.assignCallback(header, record._id, globalStore)
				);
			}

			this.isFetching = false;
		},

		deleteRecord(_id) {
			const { deleteRecordFromDB } = useRealmApp();

			if (_id) {
				const index = this.recordsData.findIndex(
					(element) => _id === element._id
				);
				this.recordsData.splice(index, 1);
				this.recordsDataBackup = this.recordsData;
				deleteRecordFromDB(this.dbName, this.colName, _id);
				this.recordsDataBackupLength = this.recordsDataLength =
					this.recordsDataLength - 1;
			} else {
				this.selectedRecords?.forEach((id) => {
					const index = this.recordsData.findIndex(
						(element) => id === element._id
					);
					this.recordsData.splice(index, 1);
					deleteRecordFromDB(this.dbName, this.colName, id);
					this.recordsDataBackupLength = this.recordsDataLength =
						this.recordsDataLength - 1;
				});
				this.recordsDataBackup = this.recordsData;
				this.selectedRecords = [];
			}
		},

		resetFilterFields() {
			this.recordsData = this.recordsDataBackup;
		},
	},
});
